import React from 'react'
import Home from '../components/Home'
import Attendence from '../components/Attendence'
import Marks from '../components/Marks'
import Todo from '../components/Todo'
import Lectures from '../components/Lectures/Index'
import { BottomNavigation, Colors } from 'react-native-paper'

export default class MyComponent extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'home', title: 'Home', icon: 'home', color: '#3498db' },
      { key: 'marks', title: 'Marks', icon: 'chart-bar', color: Colors.blueGrey400 },
      { key: 'attendence', title: 'Attendence', icon: 'run', color: '#3498db' },
      { key: 'todo', title: 'Todo', icon: 'pencil', color: Colors.blueGrey400 },
      { key: 'lectures', title: 'Lectures', icon: 'library-video', color: '#3498db' },
    ],
  };

  HomeRoute = () => <Home navigation={this.props.navigation} {...this.props} />

  MarksRoute = () => <Marks navigation={this.props.navigation} {...this.props} />

  AttendenceRoute = () => <Attendence navigation={this.props.navigation} {...this.props} />

  TodoRoute = () => <Todo navigation={this.props.navigation} {...this.props} />

  LecturesRoute = () => <Lectures navigation={this.props.navigation} {...this.props} />

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    home: this.HomeRoute,
    marks: this.MarksRoute,
    attendence: this.AttendenceRoute,
    todo: this.TodoRoute,
    lectures: this.LecturesRoute
  });

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
    );
  }
}