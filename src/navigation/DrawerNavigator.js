import React from 'react'
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer'
import BottomNavigator from './BottomNavigator'
import Logout from '../Screens/Logout'
import AsyncStorage from '@react-native-community/async-storage'
import { View } from 'react-native'
import { ActivityIndicator, Avatar, Title, Subheading, IconButton, Divider } from 'react-native-paper'
import Logo from '../Images/Logo.png'
import Animated from 'react-native-reanimated'
import SupportScreen from '../Screens/SupportScreen'

export default class DrawerNavigator extends React.Component {
  state = {
    parentData: [],
    userData: []
  }

  BottomNavigatorScreen = (props, item) => <BottomNavigator {...props} parentData={item} userData={this.state.userData} />

  componentDidMount = async () => {
    let objectID, sessionID
    objectID = await AsyncStorage.getItem('objectID')
    sessionID = await AsyncStorage.getItem('sessionID')
    Promise.all([fetch("http:www.ptmapps.com/services/MyClassService.svc/REST/parent?uid=" + objectID + "&sessionid=" + sessionID), fetch("http:www.ptmapps.com/services/MyClassService.svc/REST/user?uid=" + objectID + "&sessionid=" + sessionID)])
      .then(([res1, res2]) => {
        return Promise.all([res1.json(), res2.json()])
      })
      .then(([res1, res2]) => {
        this.setState({
          parentData: res1,
          userData: res2
        })
      });
  }


  render() {
    const Drawer = createDrawerNavigator()
    const { parentData } = this.state
    if (Object.keys(this.state.parentData).length && Object.keys(this.state.userData).length) {
      return (
        <Drawer.Navigator
          drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
          {parentData.map((item) => (
            <Drawer.Screen
              key={item}
              name={(item.firstName + ' ' + item.lastName)}
              component={(props) => this.BottomNavigatorScreen(props, item)}
              options={() => ({
                drawerIcon: () => <Avatar.Text size={30} label={item.firstName[0]} />,
                drawerLabel: () => <Subheading style={{ textTransform: 'capitalize' }}>{item.firstName + ' ' + item.lastName}</Subheading>
              })}
            />
          ))}
          <Drawer.Screen 
            name="Support"
            component={SupportScreen}
            options={() => ({
              drawerIcon: () => <IconButton icon="headset"/>,
              drawerLabel: () => <Subheading>Support</Subheading>
            })}
          />
          <Drawer.Screen
            name="Logout"
            component={Logout}
            options={() => ({
              drawerIcon: () => <IconButton icon="logout" />,
              drawerLabel: () => <Subheading>Logout</Subheading>
            })}
          />
        </Drawer.Navigator>
      )
    }
    return (
      <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" animating={true} />
      </View>
    )
  }
}

function CustomDrawerContent({ progress, ...rest }) {
  const translateX = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [-100, 0],
  });

  return (
    <Animated.View style={{ transform: [{ translateX }] }}>
      <View style={{ flexDirection: 'row', marginBottom: 10, padding: 4 }}>
        <Avatar.Image source={Logo} size={40} />
        <Title style={{ fontFamily: 'Lucida Sans Unicode', marginLeft: 4, fontWeight: "100" }}>SasiSchools</Title>
      </View>
      <Divider style={{marginBottom: 10}}/>
      <DrawerItemList {...rest} />
    </Animated.View>
  );
}
