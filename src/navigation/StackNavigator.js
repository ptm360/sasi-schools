import React, { useState } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../Screens/LoginScreen'
import OtpScreen from '../Screens/OtpScreen'
import DrawerNavigator from './DrawerNavigator'
import YouTubePlayer from '../Screens/YouTubeScreen'
import SplashScreen from '../Screens/SplashScreen'
import Logout from '../Screens/Logout'


export default function App() {
    const Stack = createStackNavigator()
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none">
                <Stack.Screen
                    name="SplashScreen"
                    component={SplashScreen}
                />
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                />
                <Stack.Screen
                    name="Otp"
                    component={OtpScreen}
                />
                <Stack.Screen
                    name="DrawerNavigator"
                    component={DrawerNavigator}
                />
                <Stack.Screen
                    name="YouTubePlayer"
                    component={YouTubePlayer}
                />
                <Stack.Screen
                    name="Logout"
                    component={Logout}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}