import React from 'react'
import Logo from '../Images/Logo.png'
import { Avatar, IconButton, Appbar, Colors } from 'react-native-paper'

export default function CustomHeader({ navigation }) {
    return(
        <Appbar.Header>
            <IconButton 
                icon="menu"
                onPress={() => navigation.openDrawer()}
            />
            <Avatar.Image source={Logo} size={52}/>
            <Appbar.Content title="Sasi Schools"/>
        </Appbar.Header>
    )
}