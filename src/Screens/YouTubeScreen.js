import React from 'react';
import YouTubePlayer from '../components/Lectures/YouTubePlayer';

const YouTubeScreen = (props) => {
    return (
        <YouTubePlayer {...props}/>
    );
};

export default YouTubeScreen;
