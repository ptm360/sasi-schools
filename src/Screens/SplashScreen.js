import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Logo from '../Images/Logo.png';
import { Avatar } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage'

const SplashScreen = (props) => {

    const navigation = async() => {
        let objectID = await AsyncStorage.getItem('objectID')
        let sessionID = await AsyncStorage.getItem('sessionID')
        setTimeout(() => props.navigation.navigate(objectID && sessionID ? 'DrawerNavigator' : 'Login'), 1000)
    }
    navigation()
    return (
        <View style={styles.container}>
            <Avatar.Image source={Logo} size={200}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default SplashScreen;
