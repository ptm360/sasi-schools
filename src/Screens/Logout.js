import React from 'react'
import { View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { useNavigation } from '@react-navigation/native'

const Logout = () => {
    const navigation = useNavigation()
    const temp = async () => {
        await AsyncStorage.getAllKeys()
            .then(keys => AsyncStorage.multiRemove(keys))
            .then(navigation.navigate('Login'))
    }
    temp()
    return <View></View>
}

export default Logout