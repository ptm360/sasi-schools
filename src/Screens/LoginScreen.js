import React from 'react'
import Login from '../components/Login'

export default function LoginScreen(props) {
    return <Login {...props} />
}