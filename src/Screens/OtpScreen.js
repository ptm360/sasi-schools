import React from 'react'
import Otp from '../components/Otp'

export default function OtpScreen(props) {
    return <Otp {...props} />
}