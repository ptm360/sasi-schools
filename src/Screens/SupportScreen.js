import React from 'react'
import Support from '../components/Support'

export default function SupportScreen(props) {
    return <Support {...props}/>
}