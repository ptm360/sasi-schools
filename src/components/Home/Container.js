import React, { Component } from 'react';
import Presentation from './Presentation';
import CustomHeader from '../../Screens/CustomHeader';

class Container extends Component {

    render() {
        return (
            <React.Fragment>
                <CustomHeader navigation={this.props.navigation}/>
                <Presentation
                    {...this.props}
                />
            </React.Fragment>
        );
    }
}

export default Container;
