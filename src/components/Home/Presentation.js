import React from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { ActivityIndicator, Card, Avatar, Text, Divider, IconButton, Subheading, Colors } from 'react-native-paper';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center'
    },
    userInfo: {
        flexDirection: 'row',
        marginBottom: 10,
        marginTop: 10
    },
    subUserInfo: {
        marginLeft: 80,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        backgroundColor: Colors.deepOrange200
    }
});

function Presentation(props) {
    const { userData, parentData } = props
    //var dob = new Date(parseInt(userData.userBday.substring(6, 19)))
    var DOB = new Date(parseInt(parentData.userBday.substring(6, 19)))
    if (Object.keys(userData).length && Object.keys(parentData).length)
        return (
            <ScrollView style={styles.container}>
                <Card>
                    <Card.Title title="User Information" />
                    <Card.Content>
                        <Divider />
                        <View style={styles.userInfo}>
                            <Avatar.Text label={parentData.firstName[0]} style={styles.avatar} />
                            <View style={styles.subUserInfo}>
                                <Subheading style={{ textTransform: 'capitalize',maxWidth: 200 }}>{parentData.firstName + ' ' + parentData.lastName}</Subheading>
                                <Subheading>{parentData.userCode}</Subheading>
                            </View>
                        </View>
                        <Divider />
                        <View style={styles.userInfo}>
                            <View>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Class</Text>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Gender</Text>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Date of birth</Text>
                                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Parent</Text>
                            </View>
                            <View style={{ marginLeft: 60 }}>
                                <Text style={{ fontSize: 16 }}>{parentData.classTitle + ' ' + parentData.classSection}</Text>
                                <Text style={{ fontSize: 16 }}>{parentData.gender}</Text>
                                <Text style={{ fontSize: 16 }}>{DOB.getDate()}/{DOB.getMonth()}/{DOB.getFullYear()}</Text>
                                <Text style={{ fontSize: 16, textTransform: 'capitalize' }}>{userData.firstName + ' ' + userData.lastName}</Text>
                            </View>
                        </View>
                        <Divider />
                        <View style={styles.userInfo}>
                            <IconButton icon="phone" />
                            <Text style={{ marginTop: 12, fontSize: 16 }}>{userData.phone}</Text>
                        </View>
                        <Divider />
                        <View style={styles.userInfo}>
                            <IconButton icon="account-badge-horizontal-outline" />
                            <View style={{ marginTop: 12 }}>
                                <Text style={{ fontSize: 16, textTransform: 'capitalize' }}>{'SRI ' + userData.firstName + ' ' + userData.lastName},</Text>
                                {userData.userAddress.line1 ? <Text style={{ fontSize: 16 }}>{userData.userAddress.line1} ,</Text> : null}
                                {userData.userAddress.line2 ? <Text style={{ fontSize: 16 }}>{userData.userAddress.line2} ,</Text> : null}
                                {userData.userAddress.line2 ? <Text style={{ fontSize: 16 }}>{userData.userAddress.line3} ,</Text> : null}
                                {userData.userAddress.city || userData.userAddress.zip ? (<Text style={{ fontSize: 16 }}>{userData.userAddress.city + ' - ' + userData.userAddress.zip} ,</Text>) : null}
                                {userData.userAddress.state || userData.userAddress.country ? (<Text style={{ fontSize: 16 }}>{userData.userAddress.state + ' , ' + userData.userAddress.country} </Text>) : null}
                            </View>
                        </View>
                    </Card.Content>
                </Card>
            </ScrollView>
        )
    return (
        <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" animating={true} />
        </View>
    )
}

export default Presentation;
