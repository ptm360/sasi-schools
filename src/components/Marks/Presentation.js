import React from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';
import { Button, Headline, DataTable, ActivityIndicator, Surface, ProgressBar } from 'react-native-paper';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    border: {
        width: '100%',
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 10,
        padding: 10,
        marginTop: 30,
        flexDirection: 'row',
        marginBottom: 10
    }
});


function Presentation(props) {
    const { units, initial, handleUnit, loading, active_ID, color } = props
    let data = []
    let percentage = parseFloat(initial[0].examTotalMarks) / parseFloat(initial[0].examMaxMarks)
    initial && initial.forEach(ele => {
        data.push({
            subjName: ele.subjName,
            subjMarks: ele.subjMarks,
            subjMaxMarks: ele.subjMaxMarks,
        })
    })
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.border}>
                    <ScrollView horizontal={true}>
                        {units.map(item => (
                            <View>
                                <Button color={(item.eID === active_ID) && color} onPress={(event) => handleUnit(event, item.eID)} mode="text">
                                    {item.examName}
                                </Button>
                            </View>
                        ))}
                    </ScrollView>
                </View>
                <Surface>
                    <DataTable>
                        <DataTable.Header>
                            <DataTable.Title>Subject</DataTable.Title>
                            <DataTable.Title numeric>Marks Obtained</DataTable.Title>
                            <DataTable.Title numeric>Maximum Marks</DataTable.Title>
                        </DataTable.Header>
                        {!loading ? data.map(item => (
                            <DataTable.Row>
                                <DataTable.Cell>{item.subjName}</DataTable.Cell>
                                <DataTable.Cell numeric>{item.subjMarks}</DataTable.Cell>
                                <DataTable.Cell numeric>{item.subjMaxMarks}</DataTable.Cell>
                            </DataTable.Row>

                        )) :
                            <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size="small" animating={true} />
                            </View>}
                    </DataTable>
                </Surface>
                {!loading && (
                    <Surface style={{ marginTop: 10, padding: 5 }}>
                        <Headline>Total: {initial[0].examTotalMarks}/{initial[0].examMaxMarks}</Headline>
                        <Text></Text>
                        <ProgressBar progress={percentage} />
                    </Surface>
                )}
            </View>
        </ScrollView>
    )
}

export default Presentation;
