import React, { Component } from 'react';
import Presentation from './Presentation';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import CustomHeader from '../../Screens/CustomHeader';

class Container extends Component {

    state = {
        units: [],
        initial: [],
        loading: false,
        active_ID: '',
        color: ''
    }

    componentDidMount = async () => {
        const { parentData } = this.props
        let sessionID = await AsyncStorage.getItem('sessionID')
        fetch("http://ptmapps.com/services/MyClassService.svc/REST/exams?intid="+parentData.institution+"&classid="+parentData.classTitle+"&uid="+parentData.userID+"&sessionid="+sessionID)
            .then(res => res.json())
            .then(data => {
                this.setState({ units: data })
                this.handleClik(data[0].eID)
            })
    }

    handleClik = async (id) => {
        const { parentData } = this.props
        let sessionID = await AsyncStorage.getItem('sessionID')
        let newColor = Colors.orange500
        fetch("http://ptmapps.com/services/MyClassService.svc/REST/report?uid="+parentData.userID.toString()+"&eid="+id+"&sessionid="+sessionID)
            .then(res => res.json())
            .then(data => {
                this.setState({ initial: data, loading: false, active_ID: id, color: newColor })
            })
    }

    handleUnit = async (event, id) => {
        event.preventDefault()
        this.setState({ loading: true })
        this.handleClik(id)
    }

    render() {
        if(Object.keys(this.state.units).length && Object.keys(this.state.initial).length)
        return (
            <React.Fragment>
                <CustomHeader navigation={this.props.navigation}/>
                <Presentation 
                    {...this.state}
                    handleUnit={this.handleUnit}
                />
            </React.Fragment>
        );
        return(
            <View style={{justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size="large" animating={true}/>
            </View>
        )
    }
}


export default Container;
