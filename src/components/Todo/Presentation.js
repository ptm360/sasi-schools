import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Card, Paragraph } from 'react-native-paper';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 8
    },
    card: {
        marginBottom: 8
    }
});

function getEndDate(id) {
    const temp = new Date(parseInt(id.substring(6, 19)))
    const date = ("0" + temp.getDate()).slice(-2)
    const month = ("0" + (temp.getMonth() + 1)).slice(-2)
    const year = temp.getFullYear();
    const today = date + '-' + month + '-' + year
    return today
}

function getDescription(description) {
    for (let i = 0; i < description.length; i++) {
        if (description[i] === '$') {
            return description.substring(0, i)
        }
    }
}

function Presentation(props) {
    const { data } = props
    return (
        <View style={styles.container}>
            {data.map(item => (
                <Card key={item} style={styles.card}>
                    <Card.Title title={item.subjName}/>
                    <Card.Content>
                        <Paragraph>Topic: {item.assignmentTopic}</Paragraph>
                        <Paragraph>Description: {getDescription(item.assignmentDescription)}</Paragraph>
                        <Paragraph>End date: {getEndDate(item.aEndDate)}</Paragraph>
                    </Card.Content>
                </Card>
            ))} 
        </View>
    )
}

export default Presentation;
