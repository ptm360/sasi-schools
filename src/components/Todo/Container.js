import React, { Component } from 'react';
import Presentation from './Presentation';
import AsyncStorage from '@react-native-community/async-storage'
import { View } from 'react-native'
import { Title, ActivityIndicator } from 'react-native-paper'
import CustomHeader from '../../Screens/CustomHeader';

class Container extends Component {

    state = {
        data: [],
        status: false
    }

    componentDidMount = async () => {
        const { parentData } = this.props
        let sessionID = await AsyncStorage.getItem('sessionID')
        var temp = new Date()
        const date = ("0" + temp.getDate()).slice(-2)
        const month = ("0" + (temp.getMonth() + 1)).slice(-2)
        const year = temp.getFullYear();
        const today = month + '-' + date + '-' + year
        fetch("http://ptmapps.com/services/MyClassService.svc/REST/assignments?uid=" + parentData.userID.toString() + "&classid=0&intid=" + parentData.institution + "&count=100&cpdate=" + today + "&sessionid=" + sessionID)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    data: json,
                    status: true
                })
                console.log(json)
            })
    }

    render() {
        const { data, status } = this.state
        if (status) {
            if (Object.keys(data).length)
                return (
                    <React.Fragment>
                        <CustomHeader navigation={this.props.navigation} />
                        <Presentation
                            {...this.state}
                        />
                    </React.Fragment>
                );
            return (
                <React.Fragment>
                    <CustomHeader navigation={this.props.navigation} />
                    <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
                        <Title>No Assignments to do</Title>
                    </View>
                </React.Fragment>
            )
        }
        return (
            <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" animating={true} />
            </View>
        )
    }
}


export default Container;
