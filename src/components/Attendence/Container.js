import React, { PureComponent } from 'react';
import Presentation from './Presentation';
import { View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { ActivityIndicator } from 'react-native-paper'
import CustomHeader from '../../Screens/CustomHeader';

class Container extends PureComponent {

    state = {
        attendence: [],
        holidays: [],
        isVisible: false,
        marked: {},
        holidayName: '',
        holidayVisible: false
    }

    componentDidMount = async () => {
        const { parentData } = this.props
        let sessionID = await AsyncStorage.getItem('sessionID')
        Promise.all([fetch("http://ptmapps.com/services/CMSService.svc/REST/sdashboard?uid=" + parentData.userID.toString() + "&sessionid=" + sessionID), fetch("http://www.ptmapps.com/services/MyClassService.svc/REST/holidays?intid=" + parentData.institution + "&sessionid=" + sessionID)])
            .then(([res1, res2]) => {
                return Promise.all([res1.json(), res2.json()])
            })
            .then(([res1, res2]) => {
                this.setState({
                    attendence: res1,
                    holidays: res2,
                    isVisible: true
                })
                this.sundays();
                this.holidays();
                this.attendenceRequests(res1.requests);
            })
    }

    sundays = () => {
        let newState = {
            weekendStyle: {
                selected: true,
                marked: true,
                dotColor: 'blue',
                selectedColor: 'gray'
            }
        }
        this.setState({ marked: newState })
    }

    holidays = () => {
        const { holidays } = this.state
        var date = [];
        var month = [];
        var year = [];
        var today = [];
        for (let index = 0; index < holidays.length; index++) {
            var temp = new Date(parseInt(holidays[index].hStartDate.substring(6, 19)));
            date[index] = ("0" + temp.getDate()).slice(-2)
            month[index] = ("0" + (temp.getMonth() + 1)).slice(-2)
            year[index] = temp.getFullYear();
            today[index] = year[index] + '-' + month[index] + '-' + date[index]
        }
        var obj = today.reduce((c, v) => Object.assign(c, {
            [v]: {
                selected: true,
                marked: true,
                selectedColor: 'gray'
            }
        }), {});
        let newstate = Object.assign(obj, this.state.marked)
        this.setState({ marked: newstate })
    }

    attendenceRequests = (attendence) => {
        var date = [];
        var month = [];
        var year = [];
        var today = [];
        for (let index = 0; index < attendence.length; index++) {
            var temp = new Date(parseInt(attendence[index].rStartDate.substring(6, 19)));
            date[index] = ("0" + temp.getDate()).slice(-2)
            month[index] = ("0" + (temp.getMonth() + 1)).slice(-2)
            year[index] = temp.getFullYear();
            today[index] = year[index] + '-' + month[index] + '-' + date[index]
        }
        var obj = today.reduce((c, v) => Object.assign(c, {
            [v]: {
                selected: true,
                marked: true,
                selectedColor: 'red'
            }
        }), {});
        let newstate = Object.assign(obj, this.state.marked)
        this.setState({ marked: newstate });
    }

    onDatePress = (props) => {
        console.log(props)
        const { holidays } = this.state
        var date = [];
        var month = [];
        var year = [];
        var today = [];
        for (let index = 0; index < holidays.length; index++) {
            var temp = new Date(parseInt(holidays[index].hStartDate.substring(6, 19)));
            date[index] = ("0" + temp.getDate()).slice(-2)
            month[index] = ("0" + (temp.getMonth() + 1)).slice(-2)
            year[index] = temp.getFullYear();
            today[index] = year[index] + '-' + month[index] + '-' + date[index]
        }
        for (let index = 0; index < today.length; index++) {
            if (props.dateString === today[index]) {
                this.setState({
                    holidayName: holidays[index].holidayName,
                    holidayVisible: true
                })
                break;
            }
        }
        this.hideModal()
    }

    hideModal = () => {
        setTimeout(() => {
            this.setState({ holidayVisible: false })
        }, 1000)
    }


    render() {
        const { isVisible } = this.state
        if (isVisible) {
            return (
                <React.Fragment>
                    <CustomHeader navigation={this.props.navigation}/>
                    <Presentation
                        {...this.state}
                        onDatePress={this.onDatePress}
                        //hideModal={this.hideModal}
                    />
                </React.Fragment>
            );
        } else {
            return (
                <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" animating={true} />
                </View>
            )
        }
    }
}


export default Container;
