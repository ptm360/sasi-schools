import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Text, ProgressBar, Divider, Headline, Surface } from 'react-native-paper';
import { CalendarList } from 'react-native-calendars'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    border: {
        borderWidth: 0.5,
        borderColor: 'red',
        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    progress: {
        marginTop: 10
    }
});

function Presentation(props) {
    const { marked, holidayName, holidayVisible, onDatePress, attendence } = props
    const percentage = attendence.attendanceAVG
    return (
        <ScrollView>
            <View style={styles.container}>
                <Surface style={{padding: 10}}>
                    <Headline>Attendence: {percentage}%</Headline>
                    <View style={styles.progress}>
                        <ProgressBar progress={parseInt(percentage) / 100} />
                    </View>
                </Surface>
                <Divider style={{ marginVertical: 20 }} />
                <View>
                    <CalendarList
                        horizontal={true}
                        markedDates={marked}
                        onDayPress={onDatePress}
                        markSunday={true}
                    />
                </View>
            </View>
            <View>
                {
                    holidayVisible ?
                        <View style={{ alignContent: 'center', marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={styles.border}>
                                <Text style={{ textAlign: 'center', textTransform: 'uppercase' }}>{holidayName}</Text>
                            </View>
                        </View>
                        : null
                }
            </View>
        </ScrollView>
    )
}

export default Presentation;
