import React from 'react'
import CustomHeader from '../../Screens/CustomHeader'
import Presentation from './Presentation'

export default class Container extends React.Component {
    render() {
        return(
            <React.Fragment>
                <CustomHeader navigation={this.props.navigation}/>
                <Presentation />
            </React.Fragment>
        )
    }
}