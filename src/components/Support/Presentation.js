import React from 'react'
import { Button, Card, Subheading } from 'react-native-paper'
import { View, StyleSheet, Linking, ScrollView } from 'react-native'

const styles = StyleSheet.create({
    container: {
        padding: 5
    },
    card: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5
    },
    cardMargin: {
        marginBottom: 10
    }
})

export default function Presentation() {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Card style={styles.cardMargin}>
                    <Card.Title title="Developed by " />
                    <Card.Content>
                        <View>
                            <Subheading>Mr.N.Sri Ram</Subheading>
                            <Subheading>Mr.N.Vyshnu</Subheading>
                            <Subheading>Mr.P.Vamsi</Subheading>
                        </View>
                    </Card.Content>
                </Card>
                <Card style={styles.cardMargin}>
                    <Card.Title title="Mentor by " />
                    <Card.Content>
                        <View style={styles.card}>
                            <Subheading>Prof P.Kiran Kumar, Computer Science and Engineering</Subheading>
                            <Subheading>Mr.M.Narendra Krishna, Vice-Chairman, SASI Educational Society</Subheading>
                        </View>
                    </Card.Content>
                </Card>
                <Card style={styles.cardMargin}>
                    <Card.Title title="Promoted by" />
                    <Card.Content>
                        <View style={styles.card}>
                            <Subheading>Computer Science and Engineering</Subheading>
                            <Subheading>Sasi Institute of Technology and Engineering</Subheading>
                            <Subheading>Tadepalligudem,West Godavari Dt,</Subheading>
                            <Subheading>Andhra Pradesh.</Subheading>
                        </View>
                    </Card.Content>
                </Card>
                <Card style={styles.cardMargin}>
                    <Card.Title title="Support" />
                    <Card.Content>
                        <View>
                            <Subheading>Contact us for your queries</Subheading>
                            <Button onPress={() => Linking.openURL('mailto:support@sasi.ac.in')} mode="text" uppercase={false}>
                                support@sasi.ac.in
                        </Button>
                        </View>
                    </Card.Content>
                </Card>
            </View>
        </ScrollView>
    )
}
