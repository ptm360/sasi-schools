import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { List } from 'react-native-paper';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30,
        backgroundColor: 'white'
    },
});

function Presentation(props) {
    const { materials, handleLink } = props
    let subjects = [...new Set(materials.map(item => item.subject))]
    //console.log(subjects)
    // let subjects = new Map(materials.map(item => [item.subject.toLowerCase(), item.subject]))
    // console.log([...subjects.values()])
    return (
        <ScrollView>
            <View>
                <List.Section title="Video Lectures">
                    <List.Accordion
                        title="Subjects"
                    //left={props => <List.Icon {...props} icon="folder" />}
                    >
                        {subjects.map(item => (
                            <List.Accordion title={item}>
                                {materials.map(element => (
                                    element.subject === item &&
                                        <List.Item
                                            title={element.topic.name}
                                            onPress={(event) => handleLink(event, element.topic.link)}
                                            left={props => <List.Icon {...props} icon="arrow-right"/>}
                                        /> 
                                ))}
                            </List.Accordion>
                        ))}
                    </List.Accordion>
                </List.Section>
            </View>
        </ScrollView>
    )
}

export default Presentation;
