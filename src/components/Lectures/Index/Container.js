import React, { Component } from 'react';
import Presentation from './Presentation';
import CustomHeader from '../../../Screens/CustomHeader';
import { ActivityIndicator } from 'react-native-paper'
import { View } from 'react-native'

class Container extends Component {

    state = {
        materials: [],
        status: false
    }

    componentDidMount = () => {
        const { parentData } = this.props
        fetch('https://us-central1-site-org-automation.cloudfunctions.net/api/v1/getMaterials', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify({
                class: parentData.classTitle
            })
        }).then(response =>
            response.json())
            .then(res => {
                this.setState({
                    materials: res.materials,
                    status: true
                })
            }
            )
    }

    handleLink = (event, link) => {
        const { navigation } = this.props
        event.preventDefault();
        navigation.navigate('YouTubePlayer', { link: link })
    }

    render() {
        const { status } = this.state
        if (status)
            return (
                <React.Fragment>
                    <CustomHeader navigation={this.props.navigation} />
                    <Presentation
                        {...this.state}
                        handleLink={this.handleLink}
                    />
                </React.Fragment>
            );
        return (
            <View style={{ justifyContent: 'center', flex: 1, alignContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" animating={true} />
            </View>
        )
    }
}


export default Container;
