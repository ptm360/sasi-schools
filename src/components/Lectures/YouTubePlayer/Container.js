import React, { Component } from 'react';
import Presentation from './Presentation';
import AsyncStorage from '@react-native-community/async-storage'

class Container extends Component {

    state = {
        
    }

    
    render() {
        return (
            <React.Fragment>
                <Presentation 
                    {...this.state}
                    {...this.props}
                />
            </React.Fragment>
        );
    }
}


export default Container;
