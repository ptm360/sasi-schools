import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import YouTube from 'react-native-youtube'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: 'black'
    },
});

function Presentation(props) {
    const { link } = props.route.params
    console.log(link)
    return (
        <View style={styles.container}>
            <YouTube
                apiKey={link}
                videoId={link}
                play
                fullscreen
                // onReady={e => this.setState({ isReady: true })}
                // onChangeState={e => this.setState({ status: e.state })}
                // onChangeQuality={e => this.setState({ quality: e.quality })}
                // onError={e => this.setState({ error: e.error })}
                style={{ alignSelf: 'stretch', height: 300 }}
            />
        </View>
    )
}

export default Presentation;
