import React, { Component } from 'react';
import Presentation from './Presentation';
import { Alert, BackHandler } from 'react-native';

class Container extends Component {

    state = {
        phone: '',
        status: false
    }

    UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        Alert.alert(
            ' Exit From App',
            'Do you want to exit from App ?',
            [
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
                { text: 'No', onPress: () => console.log('NO Pressed') }
            ],
            { cancelable: false },
        );
        return true;
    }

    handleChange = (input,event) => {
        this.setState({
            [input]: event.nativeEvent.text
        })
    }

    _login = (props) => {
        if (props.otp > 0) {
            this.props.navigation.navigate('Otp', {props: props})
            console.log('Otp is '+props.otp);
            this.clearValues()
        } else {
            Alert.alert('Error', 'Your mobile number is not registered with our school branch', [{ text: 'Ok' }])
            this.setState({ status: false })
        }
    }

    handleClick = () => {
        this.setState({ status: true })
        fetch("http://www.ptmapps.com/services/MyClassService.svc/REST/loginotp?mobileno=" + this.state.phone)
            .then(response => response.json())
            .then(data => {
                if (data.otp) this._login(data)
                else this.handleClick1()
            })
    }

    handleClick1 = () => {
        fetch("http://www.ptmapps.com/services/MyClassService.svc/REST/loginotp?mobileno=0" + this.state.phone)
            .then(response => response.json())
            .then(data => {
                this.setState({ status: true })
                if (data.otp) this._login(data)
                else this.handleClick2()
            })
    }

    handleClick2 = () => {
        fetch("http://www.ptmapps.com/services/MyClassService.svc/REST/loginotp?mobileno=+91" + this.state.phone)
            .then(response => response.json())
            .then(data => {
                this.setState({ status: true })
                this._login(data)
            })
    }


    clearValues = () => {
        this.setState({
            phone: '',
            status: false
        })
    }

    componentWillUnmount = () => {
        this.setState({
            phone: '',
            status: false
        })
    }

    render() {
        return (
            <React.Fragment>
                <Presentation 
                    {...this.state}
                    handleChange={this.handleChange}
                    handleClick={this.handleClick}
                />
            </React.Fragment>
        );
    }
}


export default Container;
