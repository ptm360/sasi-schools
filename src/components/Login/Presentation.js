import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInput, Button, Avatar, Caption, Text, Title } from 'react-native-paper';
import Logo from '../../Images/Logo.png'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30,
    },
    button: {
        marginTop: 15
    },
    avatar: {
        marginBottom: 25,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    caption: {
        marginTop: 50,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    }, 
    login: {
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        marginBottom: 25
    }
});

function Presentation(props) {
    const { phone, handleChange, handleClick, status } = props
    return (
        <React.Fragment>
            <View style={styles.container}>
                <View style={styles.avatar}>
                    <Avatar.Image source={Logo} size={100}/>
                </View>
                <View style={styles.login}>
                    <Title>Login</Title>
                </View>
                <View>
                    <TextInput
                        label="Enter your phone number"
                        value={phone}
                        onChange={(event) => handleChange('phone', event)}
                        mode='outlined'
                        keyboardType="numeric"
                        maxLength={10}
                    />
                </View>
                <View style={styles.button}>
                    <Button mode="contained" loading={status} onPress={handleClick}>
                        Sign In
                    </Button>
                </View>
                <View style={styles.caption}>
                    <Caption>Sasi Schools</Caption>
                </View>
            </View>
        </React.Fragment>
    )
}

export default Presentation;
