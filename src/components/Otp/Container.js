import React, { Component } from 'react';
import Presentation from './Presentation';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'

class Container extends Component {

    state = {
        otp: '',
        status: false
    }

    handleChange = (input,event) => {
        this.setState({
            [input]: event.nativeEvent.text
        })
    }

    componentWillUnmount = () => {
        this.setState({ otp: ''})
    }

    handleClick = async () => {
        const { props } = this.props.route.params
        this.setState({ status: true })
        if(props.otp.toString() === this.state.otp.toString()) {
            await AsyncStorage.setItem('objectID',props.objectID.toString());
            await AsyncStorage.setItem('sessionID',props.sessionID);
            this.props.navigation.navigate('DrawerNavigator', { props: props })
        } else {
            this.setState({ status: false })
            Alert.alert('Error', 'Please check the OTP', [{ text: 'Ok' }])
        }
    }

    render() {
        return (
            <React.Fragment>
                <Presentation 
                    {...this.state}
                    handleChange={this.handleChange}
                    handleClick={this.handleClick}
                />
            </React.Fragment>
        );
    }
}


export default Container;
