import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInput, Button, Avatar, Caption } from 'react-native-paper';
import Logo from '../../Images/Logo.png'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30,
        backgroundColor: 'white'
    },
    button: {
        marginTop: 15
    },
    avatar: {
        marginBottom: 50,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    caption: {
        marginTop: 50,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    }
});

function Presentation(props) {
    const { otp, handleChange, handleClick } = props
    return (
        <React.Fragment>
            <View style={styles.container}>
                <View style={styles.avatar}>
                    <Avatar.Image source={Logo} size={100}/>
                </View>
                <View>
                    <TextInput
                        label="Enter your OTP"
                        value={otp}
                        onChange={(event) => handleChange('otp', event)}
                        mode='outlined'
                        keyboardType="numeric"
                    />
                </View>
                <View style={styles.button}>
                    <Button mode="contained" loading={false} onPress={handleClick}>
                        Submit
                    </Button>
                </View>
                <View style={styles.caption}>
                    <Caption>Sasi Schools</Caption>
                </View>
            </View>
        </React.Fragment>
    )
}

export default Presentation;
