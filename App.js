import * as React from 'react';
import { configureFonts, DefaultTheme, Provider as PaperProvider, DarkTheme } from 'react-native-paper';
import Main from './src/navigation/StackNavigator';
//import { StatusBar, BackHandler, Alert } from 'react-native';

const theme = {
  ...DarkTheme,
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
  fonts: configureFonts(fontConfig)
};

const fontConfig = {
  default: {
    regular: {
      fontFamily: 'sans-serif',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'sans-serif-medium',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'sans-serif-light',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'sans-serif-thin',
      fontWeight: 'normal',
    },
  },
};


export default class App extends React.Component {

  render() {
    console.disableYellowBox = true
    return (
      <PaperProvider theme={theme}>
        {/* <StatusBar backgroundColor="#000000"  barStyle="default" /> */}
        <Main />
      </PaperProvider>
    );
  }
}